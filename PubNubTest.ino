/*
  PubNub sample client

  This sample client will use just the minimal-footprint raw PubNub
  interface where it is your responsibility to deal with the JSON encoding.

  It will just send a hello world message and retrieve one back, reporting
  its deeds on serial console.

  Circuit:
  * (Optional.) LED for reception indication.
  * (Optional.) LED for publish indication.

  created 23 October 2012
  by Petr Baudis

  https://github.com/pubnub/pubnub-api/tree/master/arduino
  This code is in the public domain.
  */

#include <SPI.h>
#include <WiFi.h>
#include <PubNub.h>
#include "DHT.h"
const int subLedPin = RED_LED;
const int pubLedPin = RED_LED;

DHT mydht(DHT11);
int16_t temperature = 0;     // Variable to return temperature from DHT11
int16_t humidity = 0;        // Variable to return humidity from DHT11
char td = 48;
char tu = 48;
char hd = 48;
char hu = 48;

char pubkey[] = "pub-c-e6689de9-b976-4a67-bb86-0655b89ae063";
char subkey[] = "sub-c-bdc6141a-4faa-11e8-8ebf-f686a6d93a6b";
char channel[] = "hello_world";

// your network name also called SSID
char ssid[] = "SmartGrids";
// your network password
char password[] = "1509Smart!Grids";


//Variables gps
int Gpsdata;            
unsigned int finish =0;  
unsigned int pos_cnt=0;  
unsigned int lat_cnt=0;  
unsigned int sp_cnt=0; 
unsigned int log_cnt=0;  
unsigned int flg    =0;  
unsigned int com_cnt=0;  
char lat[20];            
char lg[20];            
char sp[20];             
int a=0,b1=0,f=0,e=0;
float c1=0,d=0,g=0,h=0,s=0;

void Receive_GPS_Data()
{
  while(finish==0)
  {
    while(Serial1.available()>0)
    {         // Checa la informacion del GPS
      Gpsdata = Serial1.read();
      flg = 1;
     if( Gpsdata=='$' && pos_cnt == 0)   // Encontrando el header GPRMC
       pos_cnt=1;
     if( Gpsdata=='G' && pos_cnt == 1)
       pos_cnt=2;
     if( Gpsdata=='P' && pos_cnt == 2)
       pos_cnt=3;
     if( Gpsdata=='R' && pos_cnt == 3)
       pos_cnt=4;
     if( Gpsdata=='M' && pos_cnt == 4)
       pos_cnt=5;
     if( Gpsdata=='C' && pos_cnt==5 )
       pos_cnt=6;
     if(pos_cnt==6 &&  Gpsdata ==','){   // count commas in message
       com_cnt++;
       flg=0;
     }
     if(com_cnt==3 && flg==1){
      lat[lat_cnt++] =  Gpsdata;         // latitud
      flg=0;
     }

       if(com_cnt==5 && flg==1){
         lg[log_cnt++] =  Gpsdata;         // Longitud
         flg=0;
       }
       if(com_cnt==7 && flg==1){
                sp[sp_cnt++]=  Gpsdata;         // Speed
                flg=0;
              }
       if( Gpsdata == '*' && com_cnt >= 7){
         com_cnt = 0;                      // Final del mensaje de GPRMC
         lat_cnt = 0;
         log_cnt = 0;
         sp_cnt = 0;
         flg     = 0;
         finish  = 1;
      }
    }
 }
}

void setup()
{
	pinMode(subLedPin, OUTPUT);
	pinMode(pubLedPin, OUTPUT);
	digitalWrite(subLedPin, LOW);
	digitalWrite(pubLedPin, LOW);

	Serial.begin(115200);
  Serial1.begin(9600);
	Serial.println("Serial set up");

	// attempt to connect to Wifi network:
	Serial.print("Attempting to connect to Network named: ");
	// print the network name (SSID);
	Serial.println(ssid); 
	// Connect to WPA/WPA2 network. Change this line if using open or WEP network:
	WiFi.begin(ssid, password);
	while ( WiFi.status() != WL_CONNECTED) {
		// print dots while we wait to connect
		Serial.print(".");
		delay(300);
	}

	Serial.println("\nYou're connected to the network");
	Serial.println("Waiting for an ip address");

	while (WiFi.localIP() == INADDR_NONE) {
		// print dots while we wait for an ip addresss
		Serial.print(".");
		delay(300);
	}

	Serial.println("\nIP Address obtained");
	// We are connected and have an IP address.
	// Print the WiFi status.
	printWifiStatus();

	PubNub.begin(pubkey, subkey);
	Serial.println("PubNub set up");
}



void flash(int ledPin)
{
	/* Flash LED three times. */
	for (int i = 0; i < 3; i++) {
		digitalWrite(ledPin, HIGH);
		delay(100);
		digitalWrite(ledPin, LOW);
		delay(100);
	}
}

void loop()
{
  Receive_GPS_Data();
  if (mydht.readRawData(&temperature, &humidity))
  {
//    Serial.print("DHT11 Temp: ");
//    Serial.print(temperature);
//    Serial.print((char)176);                //Print degree symbol
//    Serial.print("C, Humidity: ");
//    Serial.print(humidity);
//    Serial.println("%");   
  }
//  Serial.println("En conversion de datos");
//  Serial.println("Hola");
  td=(temperature/10)+48;
  tu=(temperature%10)+48;

  hd=(humidity/10)+48;
  hu=(humidity%10)+48;
  Serial.println(td);
  Serial.println(tu);
  Serial.println(hd);
  Serial.println(hu);

  
  
	WiFiClient *client;

	Serial.println("publishing a message");
  //char lat[50]= "22.222222";
  //char lg[50]= "111.111111";
  char coord[50] = "\"20.122323, -102.323200\"";
  char weather[50] = "{\"temp\" : 20, \"hum\" : 40}";
  coord[1]=lat[0];
  coord[2]=lat[1];
  coord[4]=lat[2];
  coord[5]=lat[3];
  coord[6]=lat[5];
  coord[7]=lat[6];
  coord[8]=lat[7];
  coord[9]=lat[8];

  coord[13]=lg[0];
  coord[14]=lg[1];
  coord[15]=lg[2];
  coord[17]=lg[3];
  coord[18]=lg[4];
  coord[19]=lg[6];
  coord[20]=lg[7];
  coord[21]=lg[8];
  coord[22]=lg[9];


  weather[10]=td;
  weather[11]=tu;

   weather[22]=hd;
  weather[23]=hu;

  
	client = PubNub.publish(channel, coord);
	if (!client) {
		Serial.println("publishing error");
		delay(1000);
		return;
	}
	while (client->connected()) {
		while (client->connected() && !client->available()) ; // wait
		char c = client->read();
		Serial.print(c);
	}
	client->stop();
	Serial.println();
	flash(pubLedPin);

	Serial.println("waiting for a message (subscribe)");
	PubSubClient *pclient = PubNub.subscribe(channel);
	if (!pclient) {
		Serial.println("subscription error");
		delay(1000);
		return;
	}
	while (pclient->wait_for_data()) {
		char c = pclient->read();
		Serial.print(c);
	}
	pclient->stop();
	Serial.println();
	flash(subLedPin);

	Serial.println("retrieving message history");
	client = PubNub.history(channel);
	if (!client) {
		Serial.println("history error");
		delay(1000);
		return;
	}
	while (client->connected()) {
		while (client->connected() && !client->available()) ; // wait
		char c = client->read();
		Serial.print(c);
	}
	client->stop();
	Serial.println();

	delay(10000);






//Siguiente mensaje

client = PubNub.publish(channel, weather);
  if (!client) {
    Serial.println("publishing error");
    delay(1000);
    return;
  }
  while (client->connected()) {
    while (client->connected() && !client->available()) ; // wait
    char c = client->read();
    Serial.print(c);
  }
  client->stop();
  Serial.println();
  flash(pubLedPin);

  Serial.println("retrieving message history");
  client = PubNub.history(channel);
  if (!client) {
    Serial.println("history error");
    delay(1000);
    return;
  }
  while (client->connected()) {
    while (client->connected() && !client->available()) ; // wait
    char c = client->read();
    Serial.print(c);
  }
  client->stop();
  Serial.println();

  delay(10000);



 

 
 for (int i = 0; i<=20;i++)
  {
    Serial.print(lat[i]);
  }
  Serial.println();
  for (int i = 0; i<=20;i++)
  {
    Serial.print(lg[i]);
  }
  Serial.println();

  

  
}

void printWifiStatus() {
	// print the SSID of the network you're attached to:
	Serial.print("SSID: ");
	Serial.println(WiFi.SSID());

	// print your WiFi IP address:
	IPAddress ip = WiFi.localIP();
	Serial.print("IP Address: ");
	Serial.println(ip);

	// print the received signal strength:
	long rssi = WiFi.RSSI();
	Serial.print("signal strength (RSSI):");
	Serial.print(rssi);
	Serial.println(" dBm");

 
}


